#!/bin/bash
rm -fr order-tracking-server
rm -fr funtolearn-website
git clone git@gitlab.com:funtolearn/order-tracking-server.git
git clone git@gitlab.com:funtolearn/funtolearn-website.git

cd order-tracking-server
rm -fr order-tracking
git clone git@gitlab.com:funtolearn/order-tracking.git
cd ..

sudo docker-compose up -d --build
